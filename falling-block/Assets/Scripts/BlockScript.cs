﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Math;

public class BlockScript : MonoBehaviour
{
    public GameObject controller;

    private int xBoardPrevious = -1;
    private int yBoardPrevious = -1;
    private int xBoard = -1;
    private int yBoard = -1;
    private int blockSize = 1;

    public bool isMouseDragging;
    public Vector3 screenPosition;
    public Vector3 offset;
    public float lastMousePositionXWhenDragging;
    public float xLeftLimit = -1.755f;
    public float xRightLimit = 1.755f;
    private int dragXBoard = -1;
    public bool moveBlockWithAnimation = false;
    private string spriteName = Constants.SPRITE_NAME_BLOCK_1_RED;
    private ParticleSystem particleSystem = null;
    public bool isShaking = false;
    private bool isPowerBlock = false;

    //Reference for all sprites that block can be
    public Sprite block_4_blue, block_3_blue, block_2_blue, block_1_blue,
        block_4_orange, block_3_orange, block_2_orange, block_1_orange,
        block_4_red, block_3_red, block_2_red, block_1_red,
        block_4_green, block_3_green, block_2_green, block_1_green,
        block_4_pink, block_3_pink, block_2_pink, block_1_pink,
        block_4_power, block_3_power, block_2_power, block_1_power;

    public Material blue_material, orange_material, red_material, greenn_material, pink_material, power_material;

    public void SetController()
    {
        controller = GameObject.FindGameObjectWithTag("GameController");
    }

    //When block piece is created this activate function will be called
    public void Activate()
    {

        //Take the instantiated location and adjust the transform
        moveBlockWithAnimation = false;
        SetCoordinates();

        //Set Sprite to block object
        SetSpriteFromName();
    }

    public void SetSpriteFromName()
    {
        particleSystem = this.GetComponentInChildren<ParticleSystem>();
        ParticleSystemRenderer psr = particleSystem.GetComponent<ParticleSystemRenderer>();
        particleSystem.emission.SetBurst(0, new ParticleSystem.Burst(0f, 4 * blockSize));
        isPowerBlock = false;
        isShaking = false;
        switch (this.spriteName)
        {
            case Constants.SPRITE_NAME_BLOCK_4_BLUE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_blue;
                    psr.material = blue_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_BLUE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_blue;
                    psr.material = blue_material;
                    break;
                }
               
            case Constants.SPRITE_NAME_BLOCK_2_BLUE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_blue;
                    psr.material = blue_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_BLUE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_blue;
                    psr.material = blue_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_4_GREEN:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_green;
                    psr.material = greenn_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_GREEN:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_green;
                    psr.material = greenn_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_2_GREEN:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_green;
                    psr.material = greenn_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_GREEN:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_green;
                    psr.material = greenn_material;
                    
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_4_ORANGE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_orange;
                    psr.material = orange_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_ORANGE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_orange;
                    psr.material = orange_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_2_ORANGE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_orange;
                    psr.material = orange_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_ORANGE:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_orange;
                    psr.material = orange_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_4_RED:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_red;
                    psr.material = red_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_RED:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_red;
                    psr.material = red_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_2_RED:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_red;
                    psr.material = red_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_RED:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_red;
                    psr.material = red_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_4_PINK:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_pink;
                    psr.material = pink_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_PINK:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_pink;
                    psr.material = pink_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_2_PINK:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_pink;
                    psr.material = pink_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_PINK:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_pink;
                    psr.material = pink_material;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_4_POWER:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_4_power;
                    psr.material = power_material;
                    isPowerBlock = true;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_3_POWER:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_3_power;
                    psr.material = power_material;
                    isPowerBlock = true;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_2_POWER:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_2_power;
                    psr.material = power_material;
                    isPowerBlock = true;
                    break;
                }
            case Constants.SPRITE_NAME_BLOCK_1_POWER:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_power;
                    psr.material = power_material;
                    isPowerBlock = true;
                    break;
                }
            default:
                {
                    this.GetComponent<SpriteRenderer>().sprite = block_1_red;
                    psr.material = red_material;
                    break;
                }
        }
        Vector2 S = this.GetComponent<SpriteRenderer>().sprite.bounds.size;
        this.GetComponent<BoxCollider>().size = S;
    }

    // Funtion to set coordinates of chessPiece
    private void SetCoordinates()
    {
        float x = xBoard;
        float y = yBoard;

        if (blockSize == 2)
        {
            x += 0.5f;
        }
        else if (blockSize == 3)
        {
            x += 1;
        }
        else if (blockSize == 4)
        {
            x += 1.5f;
        }

        x *= 0.501f;
        y *= 0.501f;

        x -= 1.755f;
        y -= 3.129f;

        //if (y == -1)
        //{
        //    y -= 0.501f;
        //}
        Game game = controller.GetComponent<Game>();
        if (xBoard >= 0 && yBoard >= 0 && game.positionFilledArray[xBoard, yBoard])
        {
            Debug.Log("x =" + xBoard.ToString() + " y=" + yBoard.ToString() + " position is already filled");
        }
        SetBlockFillingPosition(false, xBoardPrevious, yBoardPrevious);
        SetBlockFillingPosition(true, xBoard, yBoard);
        //moveBlockWithAnimation = false;
        if (!moveBlockWithAnimation)
        {
            this.transform.position = new Vector3(x, y, -1);
        }
        else
        { 
            IEnumerator moveBlock = MoveBlockXY(x, y);
            StartCoroutine(moveBlock);
            moveBlockWithAnimation = false;
        }
        //xBoardPrevious = xBoard;
        //yBoardPrevious = yBoard;
    }

    IEnumerator MoveBlockXY(float x, float y)
    {
        Game game = controller.GetComponent<Game>();
        game.animatingBodyCount++;
        float speed = 0.1f;
        float timeDelta = 0.01f;
        while (this.transform.position.x < x)
        {
            if (this.transform.position.x + speed < x)
            {
                this.transform.position = new Vector3(this.transform.position.x + speed, this.transform.position.y, -1);
            }
            else {
                this.transform.position = new Vector3(x, this.transform.position.y, -1);
            }
            yield return new WaitForSeconds(timeDelta);
        }

        while (this.transform.position.x > x)
        {
            if (this.transform.position.x - speed > x)
            {
                this.transform.position = new Vector3(this.transform.position.x - speed, this.transform.position.y, -1);
            }
            else
            {
                this.transform.position = new Vector3(x, this.transform.position.y, -1);
            }
            yield return new WaitForSeconds(timeDelta);
        }

        while (this.transform.position.y < y)
        {
            if (this.transform.position.y + speed < y)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + speed , -1);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x, y, -1);
            }
            yield return new WaitForSeconds(timeDelta);
        }

        while (this.transform.position.y > y)
        {
            if (this.transform.position.y - speed > y)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - speed, -1);
            }
            else
            {
                this.transform.position = new Vector3(this.transform.position.x, y, -1);
            }
            yield return new WaitForSeconds(timeDelta);
        }
        game.animatingBodyCount--;
    }

    public void ChangeBlockPositionInXDirection(int x)
    {
        SetXBoard(x);
        SetYBoard(yBoard);
        SetCoordinates();
    }

    public void SetXMovementLimits(BlockScript blockScript)
    {
        float xBoardIndexLeftLimit = blockScript.xBoard;
        float xBoardIndexRightLimit = blockScript.xBoard;
        Game game = blockScript.controller.GetComponent<Game>();
        for (int i = blockScript.xBoard - 1; i >= 0; i--)
        {
            if (game.positionFilledArray[i, blockScript.yBoard])
            {
                break;
            }
            xBoardIndexLeftLimit = i;
        }

        for (int i = blockScript.xBoard + blockScript.blockSize; i < game.positionFilledArray.GetLength(0); i++)
        {
            if (game.positionFilledArray[i, blockScript.yBoard])
            {
                break;
            }
            xBoardIndexRightLimit = i;
        }
        if (blockScript.blockSize == 2)
        {
            xBoardIndexLeftLimit += 0.5f;
            if (xBoardIndexRightLimit == blockScript.xBoard)
            {
                xBoardIndexRightLimit += 0.5f;
            }
            else
            {
                xBoardIndexRightLimit -= 0.5f;
            }
        }
        else if (blockScript.blockSize == 3)
        {
            xBoardIndexLeftLimit += 1;
            if (xBoardIndexRightLimit == blockScript.xBoard)
            {
                xBoardIndexRightLimit += 1;
            }
            else
            {
                xBoardIndexRightLimit -= 1;
            }

        }
        else if (blockScript.blockSize == 4)
        {
            xBoardIndexLeftLimit += 1.5f;
            if (xBoardIndexRightLimit == blockScript.xBoard)
            {
                xBoardIndexRightLimit += 1.5f;
            }
            else
            {
                xBoardIndexRightLimit -= 1.5f;
            }
        }
        blockScript.xLeftLimit = xBoardIndexLeftLimit * 0.501f - 1.755f;
        blockScript.xRightLimit = xBoardIndexRightLimit * 0.501f - 1.755f;
    }

    private void SetBlockFillingPosition(bool isFilled, int x, int y)
    {
        Game game = controller.GetComponent<Game>();
        game.SetBlockFillingPosition(isFilled, x, y, blockSize);
    }

    public void MoveBlock(int distance)
    {
        //Move with animation
        SetYBoard(yBoard + distance);
        SetXBoard(xBoard);
        SetCoordinates();
    }

    public int GetXBoard()
    {
        return xBoard;
    }

    public int GetYBoard()
    {
        return yBoard;
    }

    public void SetXBoard(int x)
    {
        //xBoardPrevious = xBoard;
        //For 1st time
        if (xBoardPrevious == -1)
        {
            xBoardPrevious = x;
        }
        else {
            xBoardPrevious = xBoard;
        }
        xBoard = x;
    }

    public void SetYBoard(int y)
    {
        //yBoardPrevious = yBoard;
        //For 1st time
        if (yBoardPrevious == -1)
        {
            yBoardPrevious = y;
        }
        else {
            yBoardPrevious = yBoard;
        }
        yBoard = y;
    }

    public void ResetBlock()
    {
        yBoardPrevious = -1;
        xBoardPrevious = -1;
        xBoard = -1;
        yBoard = -1;
        blockSize = 1;
        isMouseDragging = false;
        xLeftLimit = -1.755f;
        xRightLimit = 1.755f;
        dragXBoard = -1;
        moveBlockWithAnimation = false;
        spriteName = Constants.SPRITE_NAME_BLOCK_1_RED;
    }

    public IEnumerator BreakAndRemove(bool shouldShake, bool shouldWaitforShake = false)
    {
        Game game = controller.GetComponent<Game>();
        game.animatingBodyCount++;
        isShaking = shouldShake;
        if (isShaking == true || shouldWaitforShake == true)
        {
            yield return new WaitForSeconds(0.5f);
        }
        isShaking = false;
        game.animatingBodyCount--;
        particleSystem.Play();
        this.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(particleSystem.main.startLifetime.constantMax);
        ChangeBlockPositionInXDirection(-50);
        this.GetComponent<SpriteRenderer>().enabled = true;

    }

    public int GetBlockSize()
    {
        return blockSize;
    }

    public void SetBlockSize(int size)
    {
        blockSize = size;
    }

    public int GetDragXBoard()
    {
        return dragXBoard;
    }

    public void SetDragXBoard(int x)
    {
        dragXBoard = x;
    }

    public void SetSpriteName(string name)
    {
        spriteName = name;
    }

    private void Update()
    {
        if (isShaking)
        {
            this.transform.position = new Vector3(this.transform.position.x + Random.Range(-0.004f, 0.004f),
                this.transform.position.y + Random.Range(-0.004f, 0.004f), this.transform.position.z);
        }
    }

    public bool IsPowerBlock()
    {
        return isPowerBlock;
    }
}
