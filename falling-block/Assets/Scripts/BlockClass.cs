﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockClass : MonoBehaviour
{
    public int x = -1;
    public int y = -1;
    public int blockSize = 1;
}
