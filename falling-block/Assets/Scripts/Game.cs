﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameObject block;
    public GameObject selectMoveObject;
    public ArrayList allBlocks = new ArrayList();
    public ArrayList allInvalidBlocks = new ArrayList();
    Dictionary<GameObject, BlockScript> blockScriptsMap = new Dictionary<GameObject, BlockScript>();
    public bool[,] positionFilledArray = new bool[Constants.BOARD_WIDTH,Constants.BOARD_HEIGHT];
    public ArrayList[] yBoardsBlockArrayLists = new ArrayList[Constants.BOARD_HEIGHT];
    public ArrayList nextLinesPositionsArrayList = new ArrayList();
    System.Random random = new System.Random();
    private GameObject dragTargetBlock;
    private float blockInitialPosition;
    public int animatingBodyCount = 0;
    public bool isTouchEnabled;
    private bool isNewLineGeneratorRunning = false;
    private ArrayList extraRemovalObjects = new ArrayList();
    private ArrayList lineRemovalObjects = new ArrayList();

    // Start is called before the first frame update
    void Start()
    {
        //init yBoards
        for (int i = 0; i < yBoardsBlockArrayLists.Length; i++)
        {
            yBoardsBlockArrayLists[i] = new ArrayList();
            //Debug.Log("Random Number" + Random.Range(1, 10));
        }
        //random = new System.Random();
        initGameObjects();
        GenerateNewRandomLinePositions();
        NewLineGenerator();

        IEnumerator initBoardCoroutine = InitNewBoard();
        StartCoroutine(initBoardCoroutine);
    }

    IEnumerator InitNewBoard()
    {

        IEnumerator createLineCoroutine1 = CreateLine(true);
        StartCoroutine(createLineCoroutine1);

        while (animatingBodyCount > 0)
        {
            yield return new WaitForSeconds(0.2f);
        }

        IEnumerator createLineCoroutine2 = CreateLine(true);
        StartCoroutine(createLineCoroutine2);

        while (animatingBodyCount > 0)
        {
            yield return new WaitForSeconds(0.2f);
        }

        IEnumerator createLineCoroutine3 = CreateLine(true);
        StartCoroutine(createLineCoroutine3);

        while (animatingBodyCount > 0)
        {
            yield return new WaitForSeconds(0.2f);
        }

        IEnumerator createLineCoroutine4 = CreateLine(true);
        StartCoroutine(createLineCoroutine4);

        while (animatingBodyCount > 0)
        {
            yield return new WaitForSeconds(0.2f);
        }

        IEnumerator createLineCoroutine5 = CreateLine(true);
        StartCoroutine(createLineCoroutine5);
    }

    private void GenerateNewRandomLinePositions()
    {
        bool[] positionsToFillInNewLine = PositionsToFillInNewLine();
        int blockSize;
        ArrayList newLineArrayList = new ArrayList();
        for (int x = 0; x < Constants.BOARD_WIDTH; x++)
        {
            if (positionsToFillInNewLine.Length > x && positionsToFillInNewLine[x])
            {
                blockSize = GetRandomBlockSize();
                for (int i = x; i < x + blockSize; i++)
                {
                    if (i >= positionsToFillInNewLine.Length || !positionsToFillInNewLine[i])
                    {
                        i = x;
                        blockSize = GetRandomBlockSize();
                    }
                }

                for (int i = x; i < x + blockSize; i++)
                {
                    if (i < positionsToFillInNewLine.Length)
                    {
                        positionsToFillInNewLine[i] = false;
                    }
                }
                BlockClass block = new BlockClass();
                block.blockSize = blockSize;
                block.x = x;
                block.y = -1;
                newLineArrayList.Add(block);
            }
        }
        if (newLineArrayList.Count > 0)
        {
            nextLinesPositionsArrayList.Add(newLineArrayList);
        }
    }

    private void NewLineGenerator()
    {
        if (nextLinesPositionsArrayList.Count < 20 && !isNewLineGeneratorRunning)
        { 
            //new Thread(delegate ()
            //{
                isNewLineGeneratorRunning = true;
                for (int i = nextLinesPositionsArrayList.Count; i <= 20; i++)
                {
                    GenerateNewRandomLinePositions();
                }
                isNewLineGeneratorRunning = false;
            //}).Start();
        }
    }

    private void initGameObjects()
    {
        for (int x = 0; x < Constants.BOARD_WIDTH; x++)
        {
            for (int y = 0; y < Constants.BOARD_HEIGHT + 1; y++)
            {
                GameObject gameObject = Instantiate(block, new Vector3(-100, 0, -1), Quaternion.identity);
                BlockScript blockScript = gameObject.GetComponent<BlockScript>();
                blockScript.SetController();
                blockScriptsMap[gameObject] = blockScript;
                allInvalidBlocks.Add(gameObject);
            }
        }
    }

    public void SetBlockFillingPosition(bool isFilled, int x, int y, int blockSize)
    {
        for (int i = 0; i < blockSize; i++)
        {
            if ((x + i) >= 0 && (x + i) < positionFilledArray.GetLength(0) && y < positionFilledArray.GetLength(1) && y >= 0)
            {
                positionFilledArray[x + i, y] = isFilled;
                //Debug.Log("position filled array x=" + (x + i).ToString() + " y=" + y.ToString() + " isFilled=" + isFilled.ToString());
            }
        }
    }

    private void CreateBlock(int xBoard, int yBoard, int blockSize)
    {
        string blockName = Constants.GetSpriteName(blockSize);
        GameObject gameObject = (GameObject) allInvalidBlocks[0];               //Instantiate(block, new Vector3(0, 0, -1), Quaternion.identity);
        BlockScript blockScript = blockScriptsMap[gameObject];                  //gameObject.GetComponent<BlockScript>();
        blockScript.name = blockName;
        blockScript.SetSpriteName(blockName);
        blockScript.SetXBoard(xBoard);
        blockScript.SetYBoard(yBoard);
        blockScript.SetBlockSize(blockSize);
        blockScript.Activate();
        allBlocks.Add(gameObject);
        allInvalidBlocks.Remove(gameObject);
    }

    
  

    IEnumerator CreateLine(bool withAnimation = false)
    {
        // Generate Random Line
        if (nextLinesPositionsArrayList.Count > 0)
        {
            ArrayList newLineBlocks = (ArrayList)nextLinesPositionsArrayList[0];
            nextLinesPositionsArrayList.RemoveAt(0);
            if (newLineBlocks.Count > 0)
            {
                MoveAllBlockInYDirectionByDistance(1, withAnimation);
                while (animatingBodyCount > 0)
                {
                    Debug.Log("is in infinte loop 4");
                    yield return new WaitForSeconds(0.2f);
                }
                IEnumerator dropHangingBlocksCoroutine = DropHangingBlocks(withAnimation);
                StartCoroutine(dropHangingBlocksCoroutine);
                //DropHangingBlocks(withAnimation);
                /*while (animatingBodyCount > 0)
                {
                    Debug.Log("is in infinte loop 5");
                    yield return new WaitForSeconds(0.2f);
                }
                IEnumerator removeCompletedLinesCoroutine = RemoveCompletedLines(withAnimation);
                StartCoroutine(removeCompletedLinesCoroutine);*/
                while (animatingBodyCount > 0)
                {
                    Debug.Log("is in infinte loop 7");
                    yield return new WaitForSeconds(0.2f);
                }
                foreach (BlockClass block in newLineBlocks)
                {
                    CreateBlock(block.x, -1, block.blockSize);
                }
                NewLineGenerator();
                bool shouldCreateAnotherLine = true;
                foreach (GameObject gameObject in allBlocks)
                {
                    if (gameObject.GetComponent<BlockScript>().GetYBoard() >= 0)
                    {
                        shouldCreateAnotherLine = false;
                        break;
                    }
                }
                if (shouldCreateAnotherLine)
                {
                    IEnumerator createLineCoroutine = CreateLine(true);
                    StartCoroutine(createLineCoroutine);
                }
            }
            else
            {
                GenerateNewRandomLinePositions();
                IEnumerator createLineCoroutine = CreateLine(true);
                StartCoroutine(createLineCoroutine);
            }
        }
        else
        {
            GenerateNewRandomLinePositions();
            IEnumerator createLineCoroutine = CreateLine(true);
            StartCoroutine(createLineCoroutine);
        }
    }

    private void MoveAllBlockInYDirectionByDistance(int distance, bool withAnimation = false)
    {
        foreach (GameObject gameObject in allBlocks)
        {
            BlockScript blockScript = gameObject.GetComponent<BlockScript>();
            if (withAnimation)
            {
                blockScript.moveBlockWithAnimation = true;
            }
            blockScript.MoveBlock(distance);
        }
    }

    private int GetRandomBlockSize()
    {
        int randomNumber = GetRandomNumber(1, Constants.BLOCK_MAX_SIZE + 1);
        if (randomNumber == 1 && GetRandomNumber(0, 2) == 0)
        {
            randomNumber = GetRandomNumber(1, Constants.BLOCK_MAX_SIZE + 1);
        }
        return randomNumber;
    }

    private int[] GetGapsInNewLineWithGapSize()
    {
        int randomNumber = GetRandomNumber(0, 11);
        if (randomNumber == 0)
        {
            int[] gapSizeArray = new int[3];
            gapSizeArray[0] = GetRandomGapSize();
            gapSizeArray[1] = GetRandomGapSize();
            gapSizeArray[2] = GetRandomGapSize();
            return gapSizeArray;
        }
        else if (randomNumber >= 1 && randomNumber <= 3)
        {
            int[] gapSizeArray = new int[2];
            gapSizeArray[0] = GetRandomGapSize();
            gapSizeArray[1] = GetRandomGapSize();
            return gapSizeArray;
        }
        else
        {
            int[] gapSizeArray = new int[1];
            gapSizeArray[0] = GetRandomGapSize();
            return gapSizeArray;
        }
    }

    private int GetRandomGapSize()
    {
        int randomNumber = GetRandomNumber(0, 11);//random.Next(0, 11);
        if (randomNumber == 0)
        {
            return 3;
        }
        else if (randomNumber == 1 && randomNumber == 2)
        {
            return 2;
        }
        else
        {
            return 1;
        }
    }

    private bool[] PositionsToFillInNewLine()
    {
        bool[] positionsToFill = new bool[Constants.BOARD_WIDTH];
        for (int i = 0; i < Constants.BOARD_WIDTH; i++)
        {
            positionsToFill[i] = true;
        }
        int[] gapSizeArray = GetGapsInNewLineWithGapSize();
        for (int i = 0; i < gapSizeArray.Length; i++)
        {
            int x = GetRandomXBoardPosition();
            for (int j = x; j < positionsToFill.Length && j < (x + gapSizeArray[i]); j++)
            {
                positionsToFill[j] = false;
            }
        }
        return positionsToFill;
    }

    private int GetRandomXBoardPosition()
    {
        return GetRandomNumber(0, Constants.BOARD_WIDTH);//random.Next(0, Constants.BOARD_WIDTH);
    }

    IEnumerator DropHangingBlocks(bool withAnimation = false)
    {
        CorrectFilledIndexError();
        int moveDistance;
        for (int j = 1; j < Constants.BOARD_HEIGHT; j++)
        {
            for (int i = allBlocks.Count - 1; i >= 0; i--)
            //foreach (GameObject block in allBlocks)
            {
                GameObject gameObject = (GameObject)allBlocks[i];
                BlockScript blockScript = gameObject.GetComponent<BlockScript>();
                // -1 and 0 yboard positions line in game
                if (blockScript.GetYBoard() <= 0 || j != blockScript.GetYBoard())
                {
                    continue;
                }
                moveDistance = 0;
                int x = blockScript.GetXBoard();
                int y = blockScript.GetYBoard();
                int blockSize = blockScript.GetBlockSize();
                while (IsPositionBelowBlockAvailable(x, y, blockSize))
                {
                    moveDistance++;
                    y--;
                }

                //If move distance is not zero then move it with negative move as block will drop
                if (moveDistance > 0)
                {
                    //Debug.Log("Hanging block droped with x=" + blockScript.GetXBoard().ToString()
                        //+ " from y=" + (blockScript.GetYBoard()).ToString()
                        //+ " to y=" + (blockScript.GetYBoard() - moveDistance).ToString());
                    if (withAnimation)
                    {
                        blockScript.moveBlockWithAnimation = true;
                    }
                    blockScript.MoveBlock(-moveDistance);
                }
            }
        }
        while (animatingBodyCount > 0)
        {
            Debug.Log("is in infinte loop 5");
            yield return new WaitForSeconds(0.2f);
        }
        IEnumerator removeCompletedLinesCoroutine = RemoveCompletedLines(withAnimation);
        StartCoroutine(removeCompletedLinesCoroutine);
        //Debug.Log("Drop Hanging block function called");
    }

    private bool IsPositionBelowBlockAvailable(int xBoard, int yBoard, int blockSize)
    {
        for (int i = 0; i < blockSize; i++)
        {
            if ((yBoard - 1) < 0 || positionFilledArray[xBoard + i, yBoard - 1])
            {
                return false;
            }
        }
        return true;
    }

    void Update()
    {
        
        if (isTouchEnabled)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    RaycastHit hitInfo;
                    dragTargetBlock = Constants.ReturnClickedObject(out hitInfo);
                    if (dragTargetBlock != null)
                    {
                        BlockScript blockScript = dragTargetBlock.GetComponent<BlockScript>();
                        if (blockScript.GetYBoard() < 0)
                        {
                            dragTargetBlock = null;
                            return;
                        }
                        blockScript.isMouseDragging = true;
                        blockScript.SetDragXBoard(blockScript.GetXBoard());
                        selectMoveObject.GetComponent<SelectedMove>().SetSelectedMove(blockScript.GetXBoard(), blockScript.GetBlockSize());
                        blockInitialPosition = blockScript.transform.position.x;
                        blockScript.SetXMovementLimits(blockScript);
                        //Here we Convert world position to screen position.
                        blockScript.screenPosition = Camera.main.WorldToScreenPoint(dragTargetBlock.transform.position);
                        blockScript.offset = dragTargetBlock.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, blockScript.screenPosition.y, blockScript.screenPosition.z));
                    }
                }

                // Move the cube if the screen has the finger moving.

                if (dragTargetBlock != null)
                {
                    BlockScript blockScript = dragTargetBlock.GetComponent<BlockScript>();

                    if (touch.phase == TouchPhase.Ended)
                    {
                        blockScript.isMouseDragging = false;
                        //Debug.Log("New X Position x = " + blockScript.GetDragXBoard().ToString());
                        bool isPositionChanged = false;
                        if (blockScript.GetXBoard() != blockScript.GetDragXBoard())
                        {
                            isPositionChanged = true;
                        }
                        blockScript.ChangeBlockPositionInXDirection(blockScript.GetDragXBoard());
                        selectMoveObject.GetComponent<SelectedMove>().DeselectAllMoves();
                        if (isPositionChanged)
                        {
                            StartCoroutine("PlayerMoved");
                        }
                    }

                    if (touch.phase == TouchPhase.Moved && blockScript.isMouseDragging)
                    {
                        //tracking mouse position.
                        Vector3 currentScreenSpace = new Vector3(touch.position.x, blockScript.screenPosition.y, blockScript.screenPosition.z);

                        //convert screen position to world position with offset changes.
                        Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + blockScript.offset;

                        //It will update target gameobject's current postion.
                        if (currentPosition.x < blockScript.xLeftLimit)
                        {
                            currentPosition.x = blockScript.xLeftLimit;
                        }
                        else if (currentPosition.x > blockScript.xRightLimit)
                        {
                            currentPosition.x = blockScript.xRightLimit;
                        }
                        SetDragXBoard(blockScript, currentPosition.x);
                        dragTargetBlock.transform.position = currentPosition;
                    }
                }

            }
        }
        else
        {

            if (Input.GetMouseButtonDown(0))
            {
                if (animatingBodyCount > 0)
                {
                    Debug.Log("their is some aninmating body");
                    return;
                }
                RaycastHit hitInfo;
                dragTargetBlock = Constants.ReturnClickedObject(out hitInfo);
                if (dragTargetBlock != null)
                {
                    BlockScript blockScript = dragTargetBlock.GetComponent<BlockScript>();
                    if (blockScript.GetYBoard() < 0)
                    {
                        dragTargetBlock = null;
                        return;
                    }
                    blockScript.isMouseDragging = true;
                    blockScript.SetDragXBoard(blockScript.GetXBoard());
                    selectMoveObject.GetComponent<SelectedMove>().SetSelectedMove(blockScript.GetXBoard(), blockScript.GetBlockSize());
                    blockInitialPosition = blockScript.transform.position.x;
                    blockScript.SetXMovementLimits(blockScript);
                    //Here we Convert world position to screen position.
                    blockScript.screenPosition = Camera.main.WorldToScreenPoint(dragTargetBlock.transform.position);
                    blockScript.offset = dragTargetBlock.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, blockScript.screenPosition.y, blockScript.screenPosition.z));
                }
            }
            if (animatingBodyCount > 0)
            {
                return;
            }
            if (dragTargetBlock != null)
            {
                BlockScript blockScript = dragTargetBlock.GetComponent<BlockScript>();

                if (Input.GetMouseButtonUp(0))
                {
                    blockScript.isMouseDragging = false;
                    //Debug.Log("New X Position x = " + blockScript.GetDragXBoard().ToString());
                    bool isPositionChanged = false;
                    if (blockScript.GetXBoard() != blockScript.GetDragXBoard())
                    {
                        isPositionChanged = true;
                    }
                    blockScript.ChangeBlockPositionInXDirection(blockScript.GetDragXBoard());
                    selectMoveObject.GetComponent<SelectedMove>().DeselectAllMoves();
                    if (isPositionChanged)
                    {
                        StartCoroutine("PlayerMoved");
                    }
                }

                if (blockScript.isMouseDragging)
                {
                    //tracking mouse position.
                    Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, blockScript.screenPosition.y, blockScript.screenPosition.z);

                    //convert screen position to world position with offset changes.
                    Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + blockScript.offset;

                    //It will update target gameobject's current postion.
                    if (currentPosition.x < blockScript.xLeftLimit)
                    {
                        currentPosition.x = blockScript.xLeftLimit;
                    }
                    else if (currentPosition.x > blockScript.xRightLimit)
                    {
                        currentPosition.x = blockScript.xRightLimit;
                    }
                    SetDragXBoard(blockScript, currentPosition.x);
                    dragTargetBlock.transform.position = currentPosition;
                }
            }
        }

    }

    IEnumerator PlayerMoved()
    {
        IEnumerator dropHangingBlocksCoroutine = DropHangingBlocks(true);
        StartCoroutine(dropHangingBlocksCoroutine);
        //DropHangingBlocks(true);
        /*while (animatingBodyCount > 0)
        {
            Debug.Log("is in infinte loop 3");
            yield return new WaitForSeconds(0.2f);
        }
        IEnumerator removeCompletedLinesCoroutine = RemoveCompletedLines(true);
        StartCoroutine(removeCompletedLinesCoroutine);*/
        while (animatingBodyCount > 0)
        {
            Debug.Log("is in infinte loop 2");
            yield return new WaitForSeconds(0.2f);
        }
        IEnumerator createLineCoroutine = CreateLine(true);
        StartCoroutine(createLineCoroutine);
    }

    private void SetDragXBoard(BlockScript blockScript, float newX)
    {
        float distanceTraveledInXDirection = newX - blockInitialPosition;
        int blocksTraveled = (int)System.Math.Round(distanceTraveledInXDirection / (0.501f));
        if (blockScript.GetDragXBoard() != blockScript.GetXBoard() + blocksTraveled)
        {
            selectMoveObject.GetComponent<SelectedMove>().SetSelectedMove(blockScript.GetXBoard() + blocksTraveled, blockScript.GetBlockSize());
        }
        
        blockScript.SetDragXBoard(blockScript.GetXBoard() + blocksTraveled);
    }

    IEnumerator RemoveCompletedLines(bool withAnimation = false)
    {
        // Check each line and remove
        extraRemovalObjects.Clear();
        lineRemovalObjects.Clear();
        bool isLineRemoved = false;
        for (int y = 0; y < Constants.BOARD_HEIGHT; y++)
        {
            bool isLineCompleted = true;
            for (int x = 0; x < Constants.BOARD_WIDTH; x++)
            {
                if (!positionFilledArray[x, y])
                {
                    isLineCompleted = false;
                    break;
                }
            }
            if (isLineCompleted == true)
            {
                isLineRemoved = true;
                // remove y index line
                RemoveYIndexLine(y);
            }
        }
        if (isLineRemoved)
        {
            bool isPowerBlockPresent = false;
            foreach (GameObject gameObject in extraRemovalObjects)
            {
                isPowerBlockPresent = true;
                BlockScript blockScript = gameObject.GetComponent<BlockScript>();
                SetBlockFillingPosition(false, blockScript.GetXBoard(), blockScript.GetYBoard(), blockScript.GetBlockSize());
                if (allBlocks.Contains(gameObject))
                {
                    allBlocks.Remove(gameObject);
                }
                StartCoroutine(blockScript.BreakAndRemove(true));
                blockScript.ResetBlock();
                if (!allInvalidBlocks.Contains(gameObject))
                {
                    allInvalidBlocks.Add(gameObject);
                }
            }
            foreach (GameObject gameObject in lineRemovalObjects)
            {
                BlockScript blockScript = gameObject.GetComponent<BlockScript>();
                SetBlockFillingPosition(false, blockScript.GetXBoard(), blockScript.GetYBoard(), blockScript.GetBlockSize());
                if (allBlocks.Contains(gameObject))
                {
                    allBlocks.Remove(gameObject);
                }
                StartCoroutine(blockScript.BreakAndRemove(false, isPowerBlockPresent));
                blockScript.ResetBlock();
                if (!allInvalidBlocks.Contains(gameObject))
                {
                    allInvalidBlocks.Add(gameObject);
                }
            }

            while (animatingBodyCount > 0)
            {
                Debug.Log("is in infinte loop");
                yield return new WaitForSeconds(0.2f);
            }
            IEnumerator dropHangingBlocksCoroutine = DropHangingBlocks(withAnimation);
            StartCoroutine(dropHangingBlocksCoroutine);
            //DropHangingBlocks(withAnimation);
        }
    }

    private void RemoveYIndexLine(int y)
    {
        //Debug.Log("Line removed y= " + y.ToString());
        for (int i = 0; i < allBlocks.Count; i++)
        {
            GameObject gameObject = (GameObject)allBlocks[i];
            BlockScript blockScript = gameObject.GetComponent<BlockScript>();
            if (blockScript.GetYBoard() == y)
            {
                
                //Destroy(gameObject);
                //allBlocks.Remove(gameObject);
                if (blockScript.IsPowerBlock())
                {
                    RemovePowerBlockAdjacentBlocks(blockScript, 0);
                    //StartCoroutine(blockScript.BreakAndRemove(true));
                    extraRemovalObjects.Add(gameObject);
                }
                else
                {
                    //StartCoroutine(blockScript.BreakAndRemove(false));
                    lineRemovalObjects.Add(gameObject);
                }
                //blockScript.ResetBlock();
                //allInvalidBlocks.Add(gameObject);
                //i--;
            }
        }

        //while (animatingBodyCount > 0);
    }

    private void RemovePowerBlockAdjacentBlocks(BlockScript blockScript, int checkDirection) {
        for (int j = 0; j < allBlocks.Count; j++)
        {
            GameObject gameObject1 = (GameObject)allBlocks[j];
            BlockScript blockScript1 = gameObject1.GetComponent<BlockScript>();
            if ((((checkDirection == 0 || checkDirection == -1)  && blockScript1.GetYBoard() == blockScript.GetYBoard() - 1)
                || ((checkDirection == 0 || checkDirection == 1) && blockScript1.GetYBoard() == blockScript.GetYBoard() + 1))
                && blockScript1.GetYBoard() >= 0)
            {
                if (blockScript1.GetXBoard() <= blockScript.GetXBoard()
                    && blockScript1.GetXBoard() + blockScript1.GetBlockSize() - 1 >= blockScript.GetXBoard())
                {
                    blockScript1.isShaking = true;
                    extraRemovalObjects.Add(gameObject1);
                    if (blockScript1.IsPowerBlock())
                    {
                        if (blockScript1.GetYBoard() == blockScript.GetYBoard() - 1)
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, -1);
                        }
                        else
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, 1);
                        }
                    }
                }
                else if (blockScript1.GetXBoard() <= blockScript.GetXBoard() + blockScript.GetBlockSize() - 1
                  && blockScript1.GetXBoard() + blockScript1.GetBlockSize() - 1 >= blockScript.GetXBoard() + blockScript.GetBlockSize() - 1)
                {
                    blockScript1.isShaking = true;
                    extraRemovalObjects.Add(gameObject1);
                    if (blockScript1.IsPowerBlock())
                    {
                        if (blockScript1.GetYBoard() == blockScript.GetYBoard() - 1)
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, -1);
                        }
                        else
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, 1);
                        }
                    }
                }
                else if (blockScript1.GetXBoard() >= blockScript.GetXBoard()
                    && blockScript1.GetXBoard() + blockScript1.GetBlockSize() - 1 <= blockScript.GetXBoard() + blockScript.GetBlockSize() - 1)
                {
                    blockScript1.isShaking = true;
                    extraRemovalObjects.Add(gameObject1);
                    if (blockScript1.IsPowerBlock())
                    {
                        if (blockScript1.GetYBoard() == blockScript.GetYBoard() - 1)
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, -1);
                        }
                        else
                        {
                            RemovePowerBlockAdjacentBlocks(blockScript1, 1);
                        }
                    }
                }
            }
        }
    }

    private void CorrectFilledIndexError()
    {
        ClearFillingPositionArray();
        for (int i = 0; i < allBlocks.Count; i++)
        {
            GameObject gameObject = (GameObject)allBlocks[i];
            BlockScript blockScript = gameObject.GetComponent<BlockScript>();
            SetBlockFillingPosition(true, blockScript.GetXBoard(), blockScript.GetYBoard(), blockScript.GetBlockSize());
        }
    }

    private void ClearFillingPositionArray()
    {
        for (int i = 0; i < positionFilledArray.GetLength(0); i++)
        {
            for (int j = 0; j < positionFilledArray.GetLength(1); j++)
            {
                positionFilledArray[i, j] = false;
            }
        }
    }

    public int GetRandomNumber(int min, int max)
    {
        int randomNumber = Random.Range(min, max);
        return randomNumber;
    }

}
