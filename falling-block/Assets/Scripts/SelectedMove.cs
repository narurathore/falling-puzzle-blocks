﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedMove : MonoBehaviour
{
    public GameObject xBoard0;
    public GameObject xBoard1;
    public GameObject xBoard2;
    public GameObject xBoard3;
    public GameObject xBoard4;
    public GameObject xBoard5;
    public GameObject xBoard6;
    public GameObject xBoard7;

    public void SetSelectedMove(int index, int blockSize)
    {
        // Deselect All
        DeselectAllMoves();
        for (int i = index; i < index + blockSize; i++)
        {
            switch (i)
            {
                case 0:
                    {
                        xBoard0.SetActive(true);
                        break;
                    }
                case 1:
                    {
                        xBoard1.SetActive(true);
                        break;
                    }
                case 2:
                    {
                        xBoard2.SetActive(true);
                        break;
                    }
                case 3:
                    {
                        xBoard3.SetActive(true);
                        break;
                    }
                case 4:
                    {
                        xBoard4.SetActive(true);
                        break;
                    }
                case 5:
                    {
                        xBoard5.SetActive(true);
                        break;
                    }
                case 6:
                    {
                        xBoard6.SetActive(true);
                        break;
                    }
                case 7:
                    {
                        xBoard7.SetActive(true);
                        break;
                    }
            }
        }
    }

    public void DeselectAllMoves()
    {
        xBoard0.SetActive(false);
        xBoard1.SetActive(false);
        xBoard2.SetActive(false);
        xBoard3.SetActive(false);
        xBoard4.SetActive(false);
        xBoard5.SetActive(false);
        xBoard6.SetActive(false);
        xBoard7.SetActive(false);
    }
}
