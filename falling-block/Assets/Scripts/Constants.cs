﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public const string SPRITE_NAME_BLOCK_4_BLUE = "sprite_name_block_4_blue";
    public const string SPRITE_NAME_BLOCK_3_BLUE = "sprite_name_block_3_blue";
    public const string SPRITE_NAME_BLOCK_2_BLUE = "sprite_name_block_2_blue";
    public const string SPRITE_NAME_BLOCK_1_BLUE = "sprite_name_block_1_blue";
    public const string SPRITE_NAME_BLOCK_4_GREEN = "sprite_name_block_4_green";
    public const string SPRITE_NAME_BLOCK_3_GREEN = "sprite_name_block_3_green";
    public const string SPRITE_NAME_BLOCK_2_GREEN = "sprite_name_block_2_green";
    public const string SPRITE_NAME_BLOCK_1_GREEN = "sprite_name_block_1_green";
    public const string SPRITE_NAME_BLOCK_4_ORANGE = "sprite_name_block_4_orange";
    public const string SPRITE_NAME_BLOCK_3_ORANGE = "sprite_name_block_3_orange";
    public const string SPRITE_NAME_BLOCK_2_ORANGE = "sprite_name_block_2_orange";
    public const string SPRITE_NAME_BLOCK_1_ORANGE = "sprite_name_block_1_orange";
    public const string SPRITE_NAME_BLOCK_4_RED = "sprite_name_block_4_red";
    public const string SPRITE_NAME_BLOCK_3_RED = "sprite_name_block_3_red";
    public const string SPRITE_NAME_BLOCK_2_RED = "sprite_name_block_2_red";
    public const string SPRITE_NAME_BLOCK_1_RED = "sprite_name_block_1_red";
    public const string SPRITE_NAME_BLOCK_4_PINK = "sprite_name_block_4_pink";
    public const string SPRITE_NAME_BLOCK_3_PINK = "sprite_name_block_3_pink";
    public const string SPRITE_NAME_BLOCK_2_PINK = "sprite_name_block_2_pink";
    public const string SPRITE_NAME_BLOCK_1_PINK = "sprite_name_block_1_pink";
    public const string SPRITE_NAME_BLOCK_4_POWER = "sprite_name_block_4_power";
    public const string SPRITE_NAME_BLOCK_3_POWER = "sprite_name_block_3_power";
    public const string SPRITE_NAME_BLOCK_2_POWER = "sprite_name_block_2_power";
    public const string SPRITE_NAME_BLOCK_1_POWER = "sprite_name_block_1_power";
    public const int SPRITE_COLOR_RED_CODE = 0;
    public const int SPRITE_COLOR_ORANGE_CODE = 1;
    public const int SPRITE_COLOR_BLUE_CODE = 2;
    public const int SPRITE_COLOR_PINK_CODE = 3;
    public const int SPRITE_COLOR_GREEN_CODE = 4;
    public const int SPRITE_COLOR_POWER_CODE = 5;
    public const int SPRITE_COLOR_CODE_SIZE = 6;
    public const int BOARD_WIDTH = 8;
    public const int BOARD_HEIGHT = 14;
    public const int BLOCK_MAX_SIZE = 4;


    public static string GetSpriteName(int blockSize)
    {
        //Generate Random color for block
        int colorCode = Random.Range(0, SPRITE_COLOR_CODE_SIZE);
        if (colorCode == SPRITE_COLOR_POWER_CODE && Random.Range(0, 3) != 0) {
            colorCode = Random.Range(0, SPRITE_COLOR_CODE_SIZE);
        }
        switch (blockSize)
        {
            case 4:
                switch (colorCode)
                {
                    case SPRITE_COLOR_BLUE_CODE: return SPRITE_NAME_BLOCK_4_BLUE;
                    case SPRITE_COLOR_ORANGE_CODE: return SPRITE_NAME_BLOCK_4_ORANGE;
                    case SPRITE_COLOR_RED_CODE: return SPRITE_NAME_BLOCK_4_RED;
                    case SPRITE_COLOR_PINK_CODE: return SPRITE_NAME_BLOCK_4_PINK;
                    case SPRITE_COLOR_GREEN_CODE: return SPRITE_NAME_BLOCK_4_GREEN;
                    case SPRITE_COLOR_POWER_CODE: return SPRITE_NAME_BLOCK_4_POWER;
                    default: return SPRITE_NAME_BLOCK_4_GREEN;
                }

            case 3:
                switch (colorCode)
                {
                    case SPRITE_COLOR_BLUE_CODE: return SPRITE_NAME_BLOCK_3_BLUE;
                    case SPRITE_COLOR_ORANGE_CODE: return SPRITE_NAME_BLOCK_3_ORANGE;
                    case SPRITE_COLOR_RED_CODE: return SPRITE_NAME_BLOCK_3_RED;
                    case SPRITE_COLOR_PINK_CODE: return SPRITE_NAME_BLOCK_3_PINK;
                    case SPRITE_COLOR_GREEN_CODE: return SPRITE_NAME_BLOCK_3_GREEN;
                    case SPRITE_COLOR_POWER_CODE: return SPRITE_NAME_BLOCK_3_POWER;
                    default: return SPRITE_NAME_BLOCK_3_GREEN;
                }

            case 2:
                switch (colorCode)
                {
                    case SPRITE_COLOR_BLUE_CODE: return SPRITE_NAME_BLOCK_2_BLUE;
                    case SPRITE_COLOR_ORANGE_CODE: return SPRITE_NAME_BLOCK_2_ORANGE;
                    case SPRITE_COLOR_RED_CODE: return SPRITE_NAME_BLOCK_2_RED;
                    case SPRITE_COLOR_PINK_CODE: return SPRITE_NAME_BLOCK_2_PINK;
                    case SPRITE_COLOR_GREEN_CODE: return SPRITE_NAME_BLOCK_2_GREEN;
                    case SPRITE_COLOR_POWER_CODE: return SPRITE_NAME_BLOCK_2_POWER;
                    default: return SPRITE_NAME_BLOCK_2_GREEN;
                }

            // size 1 is default case
            default:
                {
                    switch (colorCode)
                    {
                        case SPRITE_COLOR_BLUE_CODE: return SPRITE_NAME_BLOCK_1_BLUE;
                        case SPRITE_COLOR_ORANGE_CODE: return SPRITE_NAME_BLOCK_1_ORANGE;
                        case SPRITE_COLOR_RED_CODE: return SPRITE_NAME_BLOCK_1_RED;
                        case SPRITE_COLOR_PINK_CODE: return SPRITE_NAME_BLOCK_1_PINK;
                        case SPRITE_COLOR_GREEN_CODE: return SPRITE_NAME_BLOCK_1_GREEN;
                        case SPRITE_COLOR_POWER_CODE: return SPRITE_NAME_BLOCK_1_POWER;
                        default: return SPRITE_NAME_BLOCK_1_GREEN;
                    }

                }

        }
    }

    public static GameObject ReturnClickedObject(out RaycastHit hit)
    {
        GameObject targetObject = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
        {
            targetObject = hit.collider.gameObject;
            BlockScript blockScript = targetObject.GetComponent<BlockScript>();
        }
        return targetObject;
    }

    
}
